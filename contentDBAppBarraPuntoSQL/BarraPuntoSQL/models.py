from django.db import models

# Create your models here.

class Pagina(models.Model):
    nombre = models.CharField(max_length=128)
    contenido = models.CharField(max_length=12800)

    def __str__(self):
        return self.nombre + "\n" + self.contenido
#!/usr/bin/python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
import urllib.request

start_plantilla = """<!DOCTYPE html>
<html lang="en" >
<head>
<meta charset="utf-8" />
<title>Lista Parseada</title>
</head>
<body>
<h1>Lista url parseadas</h1>"""

end_plantilla = """</body>
</html>"""

urls = ""


class CounterHandler(ContentHandler):

    def __init__(self):
        self.inContent = 0
        self.theContent = ""
        self.inItem = False
        self.title = ""
        self.urls = ""

    def startElement(self, name, attrs):
        if name == 'item':
            self.inItem = True
        elif name == 'title' and self.inItem:
            self.inContent = 1
        elif name == 'link' and self.inItem:
            self.inContent = 1

    def endElement(self, name):
        if name == 'item':
            self.inItem = False
        elif name == 'title' and self.inItem:
            self.title = self.theContent
        elif name == 'link' and self.inItem:
            # print("link: " + self.theContent + ".")
            global urls
            urls = urls + '<a href="' + self.theContent + '">' + self.title + "</a><br/>"
        if self.inContent:
            self.inContent = 0
            self.theContent = ""

    def characters(self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars


def main():
    LinkParser = make_parser()
    LinkHandler = CounterHandler()
    LinkParser.setContentHandler(LinkHandler)

    # Ready, set, go!

    xmlFile = urllib.request.urlopen('http://barrapunto.com/index.rss')
    LinkParser.parse(xmlFile)
    return start_plantilla + urls + end_plantilla


if __name__ == '__main__':
    res = main()
    print(res)

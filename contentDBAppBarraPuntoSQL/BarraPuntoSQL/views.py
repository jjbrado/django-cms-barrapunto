from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Pagina
from . import xml_barrapunto
from django.shortcuts import render

# Create your views here.

form = """
    <form action="" method="POST">
    <br/>
    Contenido: <input type="text" name="Contenido" value="{contenido}">
     <input type="submit" value="Enviar">
    </form>
"""

url = str(xml_barrapunto.main())


@csrf_exempt
def index(request, nombre):
    if request.method == 'GET':
        try:
            contenido = str(Pagina.objects.get(nombre=nombre)).split('\n', 1)[1]
            response = form.format(contenido=contenido) + "<br/>" + url
            return HttpResponse(response)
        except Pagina.DoesNotExist:
            return HttpResponse(form.format(contenido="") + "<br/>" + url)
    if request.method == 'POST':
        contenido = str(Pagina(contenido=request.POST['Contenido'])).split('\n', 1)[
            1]  # Cojo el contenido del formulario
        try:
            pagina = Pagina.objects.get(nombre=nombre)
            pagina.contenido = contenido
            pagina.save()
        except Pagina.DoesNotExist:
            Pagina.objects.create(nombre=nombre, contenido=contenido)
        response = form.format(contenido=contenido) + "<br/>" + url
        return HttpResponse(response)
